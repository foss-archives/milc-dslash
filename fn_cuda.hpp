// Cuda implementation
#include <cuda_runtime.h>
#define THREADS_PER_SITE 48

#define CUCHECK(err, s) \
  if (err != cudaSuccess) { \
        printf("%s (error code %d:%s)!\n", s, err, cudaGetErrorString(err)); \
        exit(EXIT_FAILURE); \
  }

__global__ void make_back(
		su3_matrix* __restrict__ d_fat, 
		su3_matrix* __restrict__ d_lng, 
		su3_matrix*       __restrict__ d_fatbck, 
		su3_matrix*       __restrict__ d_lngbck, 
		const size_t*     __restrict__ d_bck, 
		const size_t*     __restrict__ d_bck3, 
		int         total_even_sites)
{
  int myThread = blockDim.x * blockIdx.x + threadIdx.x;
  int mySite = myThread;
  if (mySite < total_even_sites) {
    for(int dir = 0; dir < 4; dir++) {
      su3_matrix *a = d_fat + 4*d_bck[4*mySite+dir]+dir;
      su3_matrix *b = d_fatbck + 4*mySite+dir;
      for(int i=0;i<3;i++)for(int j=0;j<3;j++){
          CONJG( a->e[j][i], b->e[i][j] );
      }
 
      a = d_lng + 4*d_bck3[4*mySite+dir]+dir;
      b = d_lngbck + 4*mySite+dir;
      for(int i=0;i<3;i++)for(int j=0;j<3;j++){
          CONJG( a->e[j][i], b->e[i][j] );
      }
      //su3_adjoint( d_fat + 4*d_bck[4*mySite+dir]+dir, d_fatbck + 4*mySite+dir );
      //su3_adjoint( d_lng + 4*d_bck3[4*mySite+dir]+dir, d_lngbck + 4*mySite+dir );
    }
  }
}
	    
__global__ void dslash(
		const su3_matrix* __restrict__ d_fat, 
		const su3_matrix* __restrict__ d_lng, 
		const su3_matrix* __restrict__ d_fatbck, 
		const su3_matrix* __restrict__ d_lngbck, 
		const su3_vector* __restrict__ d_src, 
		su3_vector*       __restrict__ d_dst, 
		const size_t*     __restrict__ d_fwd, 
		const size_t*     __restrict__ d_fwd3, 
		const size_t*     __restrict__ d_bck, 
		const size_t*     __restrict__ d_bck3, 
		int               total_even_sites)
{

  int myThread = blockDim.x * blockIdx.x + threadIdx.x;
  int mySite = myThread/THREADS_PER_SITE;
  
  //threadsPerBlock=48,96 -> must be a multiple of 48 and divisible by total_even_sites * THREADS_PER_SITE
#if OPTION == 1
  int k = myThread%4;
  int row = (myThread/4)%3;
#else
  int k = (myThread/3)%4;
  int row = myThread%3;
#endif
  int mat = (myThread/12)%4;
  
  extern __shared__ Complx v[];

  if (mySite < total_even_sites) {

    if (mat == 0) {
      auto *a = d_fat + mySite*4 + k;
      auto *b = d_src + d_fwd[4*mySite + k];
      Complx x = {0.0, 0.0};
      for(int j=0;j<3;j++){
        CMULSUM( a->e[row][j] , b->c[j] , x );
      }
      v[threadIdx.x].real = x.real;
      v[threadIdx.x].imag = x.imag;
    }

    else if (mat == 1) {
      auto *a = d_lng + mySite*4 + k;
      auto *b = d_src + d_fwd3[4*mySite + k];
      Complx x = {0.0, 0.0};
      for(int j=0;j<3;j++){
        CMULSUM( a->e[row][j] , b->c[j] , x );
      }
      v[threadIdx.x].real = x.real;
      v[threadIdx.x].imag = x.imag;
    }
    else if (mat == 2) { 
      auto *a = d_fatbck + mySite*4 + k;
      auto *b = d_src + d_bck[4*mySite + k];
      Complx x = {0.0, 0.0};
      for(int j=0;j<3;j++){
        CMULSUM( a->e[row][j] , b->c[j] , x );
      }
      v[threadIdx.x].real = x.real;
      v[threadIdx.x].imag = x.imag;
    }
    else if (mat == 3) {
      auto *a = d_lngbck + mySite*4 + k;
      auto *b = d_src + d_bck3[4*mySite + k];
      Complx x = {0.0, 0.0};
      for(int j=0;j<3;j++){
        CMULSUM( a->e[row][j] , b->c[j] , x );
      }
      v[threadIdx.x].real = x.real;
      v[threadIdx.x].imag = x.imag;
    }

    __syncthreads();

    if( mat == 0) { 
      v[threadIdx.x].real = v[threadIdx.x].real + v[threadIdx.x+12].real - v[threadIdx.x+24].real - v[threadIdx.x+36].real;
      v[threadIdx.x].imag = v[threadIdx.x].imag + v[threadIdx.x+12].imag - v[threadIdx.x+24].imag - v[threadIdx.x+36].imag;
    }

    __syncthreads();

    if( mat == 0 && k == 0) { 
#if OPTION == 1
      d_dst[mySite].c[row].real = v[threadIdx.x].real + v[threadIdx.x+1].real + v[threadIdx.x+2].real + v[threadIdx.x+3].real;
      d_dst[mySite].c[row].imag = v[threadIdx.x].imag + v[threadIdx.x+1].imag + v[threadIdx.x+2].imag + v[threadIdx.x+3].imag;
#else
      d_dst[mySite].c[row].real = v[threadIdx.x].real + v[threadIdx.x+3].real + v[threadIdx.x+6].real + v[threadIdx.x+9].real;
      d_dst[mySite].c[row].imag = v[threadIdx.x].imag + v[threadIdx.x+3].imag + v[threadIdx.x+6].imag + v[threadIdx.x+9].imag;
#endif
    }
 
  }
}


double dslash_fn(
  const std::vector<su3_vector> &src, 
        std::vector<su3_vector> &dst,
  const std::vector<su3_matrix> &fat,
  const std::vector<su3_matrix> &lng,
        std::vector<su3_matrix> &fatbck,
        std::vector<su3_matrix> &lngbck,
  size_t *fwd, size_t *bck, size_t *fwd3, size_t *bck3,    
  const size_t iterations,
  size_t threadsPerBlock )
{ 

  if (threadsPerBlock == 0)
  threadsPerBlock = THREADS_PER_SITE;
  
  // Device initialization
  int deviceCount;
  cudaGetDeviceCount(&deviceCount);
  if (deviceCount == 0) {
    printf("ERROR: No devices found\n");
    exit(1);
  }

  struct cudaDeviceProp device_prop;
  if (verbose >= 3) {
    for (int i = 0; i < deviceCount; ++i) {
      cudaGetDeviceProperties(&device_prop, i);
      printf("Located device %d: %s\n", i, device_prop.name);
    }
  }

  int total_sites = sites_on_node; 
  int total_even_sites = even_sites_on_node;
  
  auto copy_start = Clock::now();
  // allocate device memory
  cudaError_t cuErr;
  su3_vector *d_src, *d_dst;
  su3_matrix *d_fat, *d_lng, *d_fatbck, *d_lngbck;
  cuErr = cudaMalloc((void **)&d_src, total_sites * 1 * sizeof(su3_vector));
  CUCHECK(cuErr, "Unable to allocate array d_src");
  cuErr = cudaMalloc((void **)&d_dst, total_sites * 1 * sizeof(su3_vector));
  CUCHECK(cuErr, "Unable to allocate array d_dst");
  cuErr = cudaMalloc((void **)&d_fat, total_sites * 4 * sizeof(su3_matrix));
  CUCHECK(cuErr, "Unable to allocate array d_fat");
  cuErr = cudaMalloc((void **)&d_lng, total_sites * 4 * sizeof(su3_matrix));
  CUCHECK(cuErr, "Unable to allocate array d_lng");
  cuErr = cudaMalloc((void **)&d_fatbck, total_sites * 4 * sizeof(su3_matrix));
  CUCHECK(cuErr, "Unable to allocate array d_fatbck");
  cuErr = cudaMalloc((void **)&d_lngbck, total_sites * 4 * sizeof(su3_matrix));
  CUCHECK(cuErr, "Unable to allocate array d_lngbck");

  size_t *d_fwd, *d_bck, *d_fwd3, *d_bck3;
  cuErr = cudaMalloc((void **)&d_fwd, total_sites * 4 * sizeof(size_t));
  CUCHECK(cuErr, "Unable to allocate array d_fwd");
  cuErr = cudaMalloc((void **)&d_bck, total_sites * 4 * sizeof(size_t));
  CUCHECK(cuErr, "Unable to allocate array d_bck");
  cuErr = cudaMalloc((void **)&d_fwd3, total_sites * 4 * sizeof(size_t));
  CUCHECK(cuErr, "Unable to allocate array d_fwd3");
  cuErr = cudaMalloc((void **)&d_bck3, total_sites * 4 * sizeof(size_t));
  CUCHECK(cuErr, "Unable to allocate array d_bck3");

  size_t bytes  = sizeof(size_t)*total_sites*4;
  cudaMemcpy(d_fat, fat.data(), fat.size() * sizeof(su3_matrix), cudaMemcpyHostToDevice);
  cudaMemcpy(d_lng, lng.data(), lng.size() * sizeof(su3_matrix), cudaMemcpyHostToDevice);
  cudaMemcpy(d_bck, bck, bytes, cudaMemcpyHostToDevice);
  cudaMemcpy(d_bck3, bck3, bytes, cudaMemcpyHostToDevice);

  cudaMemcpy(d_src, src.data(), src.size() * sizeof(su3_vector), cudaMemcpyHostToDevice);
  cudaMemcpy(d_fwd, fwd, bytes, cudaMemcpyHostToDevice);
  cudaMemcpy(d_fwd3, fwd3, bytes, cudaMemcpyHostToDevice);

  double copy_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-copy_start).count();
  if (verbose > 1) {
    std::cout << "Time to offload input data = " << copy_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }

  // Create backward links on the device
  size_t blocksPerGrid = total_even_sites;
  //int blocksPerGrid = total_even_sites + 0.999999;
  if (verbose > 1) {
    std::cout << "Creating backward links"  << std::endl;
    std::cout << "Setting number of work items " << blocksPerGrid << std::endl;
    std::cout << "Setting workgroup size to " << 1 << std::endl;
  }
  auto back_start = Clock::now();
  make_back<<<blocksPerGrid, 1>>>(d_fat, d_lng, d_fatbck, d_lngbck, d_bck, d_bck3, total_even_sites);
  cudaDeviceSynchronize();
  double back_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-back_start).count();
  CUCHECK(cudaGetLastError(), "make_back kernel Failed");
  if (verbose > 1) {
    std::cout << "Time to create back links = " << back_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }
  
  // Dslash benchmark loop
  blocksPerGrid = (total_even_sites * THREADS_PER_SITE)/(double)threadsPerBlock;
  //blocksPerGrid = (total_even_sites * THREADS_PER_SITE)/(double)threadsPerBlock + 0.999999;
  if (verbose > 0) {
    std::cout << "Running dslash loop" << std::endl;
    std::cout << "Setting number of work items to " << total_even_sites * THREADS_PER_SITE << std::endl;
    std::cout << "Setting number of groups to " << blocksPerGrid << std::endl;
    std::cout << "Setting workgroup size to " << threadsPerBlock << std::endl;
  }
  auto tstart = Clock::now();
  for (int iters=0; iters<iterations+warmups; ++iters) {
    if (iters == warmups) {
      cudaDeviceSynchronize();
      tstart = Clock::now();
    } 
    // Dslash kernel
    dslash<<<blocksPerGrid, threadsPerBlock, threadsPerBlock * sizeof(Complx)>>>(d_fat, d_lng, d_fatbck, d_lngbck, d_src, d_dst, d_fwd, d_fwd3, d_bck, d_bck3, total_even_sites);
    cudaDeviceSynchronize();
  } // end of iteration loop
  double ttotal = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-tstart).count();
  CUCHECK(cudaGetLastError(), "dslash kernel Failed");
    
  // Move the result back to the host
  copy_start = Clock::now();
  cudaMemcpy(dst.data(), d_dst, dst.size() * sizeof(su3_vector), cudaMemcpyDeviceToHost);
  // Copy backward links to host for use in validation
  cudaMemcpy(fatbck.data(), d_fatbck, fatbck.size() * sizeof(su3_matrix), cudaMemcpyDeviceToHost);
  cudaMemcpy(lngbck.data(), d_lngbck, lngbck.size() * sizeof(su3_matrix), cudaMemcpyDeviceToHost);
  copy_time = std::chrono::duration_cast<std::chrono::microseconds>(Clock::now()-copy_start).count();
  if (verbose > 1) {
    std::cout << "Time to offload backward links = " << copy_time/1.0e6 << " secs\n";
    std::cout << std::flush;
  }

  cudaFree(d_src);
  cudaFree(d_dst);    
  cudaFree(d_fat);    
  cudaFree(d_lng);    
  cudaFree(d_fatbck); 
  cudaFree(d_lngbck); 

  cudaFree(d_fwd);  
  cudaFree(d_bck);  
  cudaFree(d_fwd3); 
  cudaFree(d_bck3); 

  return (ttotal /= 1.0e6);
}
