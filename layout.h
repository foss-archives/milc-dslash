#ifndef _LAYOUT_H
#define _LAYOUT_H

#include <unistd.h>
#include <vector>
#include <stdlib.h>

static int nx = LDIM;
static int ny = LDIM;
static int nz = LDIM;
static int nt = LDIM;

#define XUP 0
#define YUP 1
#define ZUP 2
#define TUP 3

//--------------------------------------------------------------------------------
inline size_t node_index(const int x, const int y, const int z, const int t) 
{
  size_t i;
  int xr,yr,zr,tr;
  xr = (x+nx)%squaresize[XUP]; yr = (y+ny)%squaresize[YUP];
  zr = (z+nz)%squaresize[ZUP]; tr = (t+nt)%squaresize[TUP];
  i = xr + squaresize[XUP]*( yr + squaresize[YUP]*( zr + squaresize[ZUP]*tr));
  if( (x+y+z+t)%2==0 ){	/* even site */
    return( i/2 );
  }
  else {
    return( (i + sites_on_node)/2 );
  }
}

//--------------------------------------------------------------------------------
// Set the indices for gathers from neighbors
inline void set_neighbors( size_t *fwd, size_t *bck, 
			   size_t *fwd3, size_t *bck3 )
{
  for(int x = 0; x < nx; x++)
    for(int y = 0; y < ny; y++)
      for(int z = 0; z < nz; z++)
	for(int t = 0; t < nt; t++)
	  {
            int i = node_index(x,y,z,t);
	    fwd[4*i+0]  = node_index(x+1,y,z,t);
	    bck[4*i+0]  = node_index(x-1,y,z,t);
	    fwd[4*i+1]  = node_index(x,y+1,z,t);
	    bck[4*i+1]  = node_index(x,y-1,z,t);
	    fwd[4*i+2]  = node_index(x,y,z+1,t);
	    bck[4*i+2]  = node_index(x,y,z-1,t);
	    fwd[4*i+3]  = node_index(x,y,z,t+1);
	    bck[4*i+3]  = node_index(x,y,z,t-1);
	    fwd3[4*i+0] = node_index(x+3,y,z,t);
	    bck3[4*i+0] = node_index(x-3,y,z,t);
	    fwd3[4*i+1] = node_index(x,y+3,z,t);
	    bck3[4*i+1] = node_index(x,y-3,z,t);
	    fwd3[4*i+2] = node_index(x,y,z+3,t);
	    bck3[4*i+2] = node_index(x,y,z-3,t);
	    fwd3[4*i+3] = node_index(x,y,z,t+3);
	    bck3[4*i+3] = node_index(x,y,z,t-3);
	  }
}

#endif
